import psycopg2
import pandas as pd
import dask.dataframe as dd
import pyarrow.parquet as pq
import pyarrow as pa
import numpy as np
import configparser
import sys
import subprocess
import datetime as dt
# import feather
import os
import sys
import xlsxwriter
import gc
import pickle
from sklearn.pipeline import Pipeline

def get_feature_names_mixed_types(column_transformer, cat_feats=None):    
    col_name = []
    for transformer_in_columns in column_transformer.transformers_[:-1]:#the last transformer is ColumnTransformer's 'remainder'
        raw_col_name = transformer_in_columns[2]
        if isinstance(transformer_in_columns[1],Pipeline): 
            transformer = transformer_in_columns[1].steps[-1][1]
        else:
            transformer = transformer_in_columns[1]
        try:
            names = transformer.get_feature_names()
        except AttributeError: # if no 'get_feature_names' function, use raw column name
            names = raw_col_name
        if isinstance(names,np.ndarray): # eg.
            col_name += names.tolist()
        elif isinstance(names,list):
            col_name += names    
        elif isinstance(names,str):
            col_name.append(names)
    if cat_feats is not None:
        for i in range(len(cat_feats)):
            cat_feat = cat_feats[i]
            col_name = [col.replace('x{}_'.format(i), '{}||'.format(cat_feat)) for col in col_name]
    return col_name


def get_rank_by_group(df, group_name, value_name, ascending):
    # Group by columns and sort values based on value column, add a percent rank column
    if ascending:
        order_idx = np.lexsort((df[value_name].values, df[group_name].values))
    else:
        order_idx = np.lexsort((-df[value_name].values, df[group_name].values))
    out = df.iloc[order_idx, :].copy()
    tmp = out.groupby(group_name).size()
    rank = tmp.map(range)
    out['rank'] = np.fromiter(
        ((item + 1) / sublist.stop * 100 for sublist in rank for item in
         sublist), np.float32)
    out.sort_index(inplace=True)
    return out['rank'].values

def notify(title, text):
    # Need sudo access
    os.system("""
              osascript -e 'display notification "{}" with title "{}"'
              """.format(text, title))

def get_file_from_gcp(from_path, to_path=1, transfer_only=False):
    from_path_split = from_path.split('/')
    if str(to_path).isdigit(): # integer
        if to_path == 1:
            to_path_suffix = from_path_split[-1]
        elif to_path > 1:
            to_path_suffix = '/'.join(from_path_split[-to_path-1:-1])
        to_path = to_path_suffix
    if not (to_path.endswith('.parquet') or to_path.endswith('.pq')):
        subprocess.call(['mkdir', to_path])
        read_path = to_path + from_path_split[-1] + '/'
    else:
        read_path = to_path
    print('From:', from_path)
    print('To:', to_path)
    print('Read:', read_path)
    if not os.path.exists(read_path):
        print('gsutil -m cp -r', from_path, to_path)
        print('Transferring ...')
        subprocess.call(['gsutil', '-m','cp', '-r', from_path, to_path])
    else:
        print('File exists!')
    if not transfer_only:
        df = pq.read_table(read_path).to_pandas()
        return df


def tic():
    global ticdt
    ticdt = dt.datetime.now()
    
def toc(msg='Elapsed time'):
    time = dt.datetime.now() - ticdt
    print('\n{0}: {1} \n'.format(msg, time))

def create_redshift_conn(cred_path):
    config = configparser.ConfigParser()
    config.read(cred_path)
    config.sections()
    credentials = config['PROD']
    # insert your own credentials here. here are the credentials for prod
    dbname = credentials['dbname']
    host = credentials['host']
    port = credentials['port']
    user = credentials['user']
    password = credentials['password']
    conn = psycopg2.connect("dbname=%s host=%s port=%s user=%s password=%s" % (dbname,host,port,user,password))
    return conn

def get_redshift_df(cred_path, query=""):
    conn = create_redshift_conn(cred_path)
    # create cursor
    cur = conn.cursor()
    # run query, get results
    cur.execute(query)
    df = cur.fetchall()
    # convert results to pandas df
    df = pd.DataFrame(df, columns=[i[0] for i in cur.description])
    # close connection, cursor
    cur.close()
    conn.close()
    return df

def perform_redshift_operations(cred_path, query_list=[]):
    conn = create_redshift_conn(cred_path)
    for query in query_list:
        # create cursor
        cur = conn.cursor()
        # run query, get results
        cur.execute(query)
        conn.commit()
    cur.close()
    conn.close()
    return None

def load_parq_or_run_query_save(file_name, cred_path, query):
    if os.path.exists(file_name):
        df = pq.read_table(file_name).to_pandas()
    else:
        df = get_redshift_df(cred_path, query)
        pd.DataFrame(df).to_parquet(file_name, engine='pyarrow')
    print(df.shape)
    return df

# create pickle_item function
def pickle_item(item, pkl_file):
    # open file, pickle and close
    pickle_file = open(pkl_file, 'wb') 
    pickle.dump(item, pickle_file)                      
    pickle_file.close()
    
# create unpickle_item function
def unpickle_item(pkl_file):
    # open file, unpickle and close
    pickle_file = open(pkl_file, 'rb') 
    item = pickle.load(pickle_file)                      
    pickle_file.close()
    return(item)

def recombined_cat_columns(data, cat_col_prefix_list, drop_original=True):
    """
    Find categorical columns with prefix in the prefix list and combine them
    """
    df_out = pd.DataFrame()
    all_cat_cols = []
    for cat_col_prefix in cat_col_prefix_list:
        cat_cols = [col for col in df.columns if col.startswith(cat_col_prefix)]
        all_cat_cols.append(cat_cols)
        x = df[cat_cols].stack()
        pd_series = pd.Series(pd.Categorical(x[x!=0].index.get_level_values(1))).str.replace(cat_col_prefix, '')
        df_col = pd_series.to_frame(name=cat_col_prefix[:-1])
        df_out = pd.concat([df_out, df_col], axis=1)
    all_cat_cols = [item for sublist in all_cat_cols for item in sublist]
    if not drop_original:
        df_final = pd.concat([data, df_out], axis=1).drop(all_cat_cols, axis=1)
        return df_final
    else:
        return df_out

def transform_columns(df, dict_transforms):
    """
    Transform columns in data according to key value pairs in function dictionary.
    """
    df_out = pd.DataFrame()
    for col, func in dict_transforms.items():
        df_col = df[col].map(func)
        df_out = pd.concat([df_out, df_col], axis=1)
    return df_out

def train_test_split_by_time(X, y, time_col, split_time, drop_time_col=True):
    """
    Refer to sklearn's train_test_split.
    Input includes a time column name and a time cutoff to split on.
    """
    assert time_col in X.columns
    intime_idx = X.index[X[time_col] < split_time].tolist()
    outtime_idx = list(set(X.index) - set(intime_idx))
    X_train, X_test, y_train, y_test = X.iloc[intime_idx, :], X.iloc[outtime_idx, :], y.iloc[intime_idx, :], y.iloc[outtime_idx, :]
    X_train, X_test = X_train.drop(time_col, axis=1), X_test.drop(time_col, axis=1)
    return  X_train, X_test, y_train, y_test

# data transfer/conversion
def transfer_data_csv(aws_dir=None,gcp_dir=None,local_dir=None,data_fname=None,delim=','):
    
    assert data_fname is not None, "Data name must not be empty"
    assert '.csv' in data_fname, "Data must be of type CSV"
    
    # copy from s3 to local
    print(f'Copying over from S3: {aws_dir}/{data_fname} to {local_dir}/')
    subprocess.call(f'gsutil cp {aws_dir}/{data_fname} {local_dir}/',shell=True)

    # read in dataframe (csv)
    print(f'Reading file from: {data_fname}...')
    df = pd.read_csv(f'{local_dir}/{data_fname}',delimiter=delim)
    
    # handle 'Null'
    df = df.apply(lambda col: col.replace(['Null','NULL','null'],np.nan) if col.dtype=='O' else col)
    # handle post_cd column
    df = df.apply(lambda col: col.astype(float) if col.name in ['primary_dlvry_addr_post_cd'] else col)
    
    # save as parquet
    fname_save = data_fname.replace('.csv','.pq')
    print(f'Converting to parquet to save as: {fname_save}...')
    df.to_parquet(f'{local_dir}/{fname_save}', index=False)

    # remove file
    os.remove(f'{local_dir}/{data_fname}')
    
    # copy over to gcp bucket files from local
    print(f'Copying over from local to GCS: {gcp_dir}/{fname_save}...')
    subprocess.call(f'gsutil cp {local_dir}/{fname_save} {gcp_dir}/',shell=True)