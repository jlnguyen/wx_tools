# ==============================================================
'''
SCRIPT:
	wx_visualise.py
	
PURPOSE:
    - WooliesX plotting tools
    
INPUTS:
	
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 17Jun2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go

# # Rotate x-ticks
# ax = sns.countplot(x='lifestage', hue='send_catg', data=send)
# a = plt.setp(
#     ax.get_xticklabels(), rotation=45, ha="right",
#     rotation_mode="anchor"
# )


def plot_dual_metrics(
    df, x='scr_gradient_ub', y1='score', y2='target_cost',
    linewidth=2,
    y1_color='tab:blue', y2_color='tab:orange',
    title=None, line_x0=False,
    fig=None, ax=None,
    x_opt=None, y_opt=None, text_opt=None, text_y_pos=None,
#     **kwargs
):
    '''Plot metric y1 vs x, and y2 vs x (dual axis)
    Args:
        y_opt (series): column-value to print
    '''
#     sns.set_palette(sns.color_palette("muted", 10))

    if fig is None:
        fig, ax = plt.subplots()
            
    sns.lineplot(data=df, x=x, y=y1, ax=ax,
                 color=y1_color, label=y1.capitalize(),
                 linewidth=linewidth)
    if line_x0:
        ax.axvline(x=0, c='k', ls='--', linewidth=linewidth)
    
    if y2 is not None:
        ax2 = ax.twinx()
        sns.lineplot(data=df, x=x, y=y2, ax=ax2,
                     color=y2_color, label=y2.capitalize(),
                     linewidth=linewidth)
    else:
        ax2 = None
    
    # legend
    ax.legend(loc='upper center', bbox_to_anchor=(1.3, 1))
    
    if y2 is not None:
        ax2.legend(loc='upper center', bbox_to_anchor=(1.3, 0.89))

    if title is None:
        ax.set_title(
            f'{y1.capitalize()} and {y2.capitalize()} vs {x.capitalize()}')
    else:
        ax.set_title(title)

    # x optimal
    if x_opt is not None:
        ax.axvline(x=x_opt, c='lawngreen', ls='--', lw=3)

        if text_opt is None:
            text_opt = f'{x}*: {np.round(x_opt,2)}'
            if y_opt is not None:
                for j in range(len(y_opt)):
                    text_opt += f'\n{y_opt.index[j]}: {y_opt[j].round(2)}'

        x_lims = ax.get_xlim()
        x_range = x_lims[1] - x_lims[0]
        x_delta = x_range * 0.05
        x_text_pos = x_opt + x_delta

        y_lims = ax.get_ylim()
        y_range = y_lims[1] - y_lims[0]
        y_delta = y_range * 0.05
        if text_y_pos is None:
            text_y_pos = (y_lims[0] + 0.8 * y_range) + y_delta
        
        ax.text(x=x_text_pos, y=text_y_pos, s=text_opt,
                 fontsize=14, verticalalignment='top',
                 bbox=dict(facecolor='yellow', alpha=0.3))

    return fig, ax, ax2


def plot_x_volume_to_percent(ax, q_size, q_pct):
    '''Convert x-axis volume into percent and plot
    '''
    ax3 = ax.twiny()
    map_vol_to_pct = lambda v: v / q_size * q_pct
    newlabel = [map_vol_to_pct(v).round(2) for v in ax.get_xticks()]
    map_pct_to_vol = lambda p: p * q_size / q_pct
    newpos   = [map_pct_to_vol(x) for x in newlabel]
    ax3.set_xticks(newpos)
    ax3.set_xticklabels(newlabel)
    ax3.xaxis.set_ticks_position('bottom')
    ax3.xaxis.set_label_position('bottom')
    ax3.spines['bottom'].set_position(('outward', 50))
    ax3.set_xlabel('volume %')
    ax3.set_xlim(ax.get_xlim())
    return None


def plot_decile_stacked_bar(
    pvt, grp_ls,
    fig=None, ax=None,
    title=None, #'Group by Decile',
    y_label=None, #'crn volume',
    legend_x_pos=1.15, legend_y_pos=1,
):
    '''Plot deciles with group values stacked
    '''
    
    if fig is None:
        fig, ax = plt.subplots()
    
    # Color
    n_grp = len(grp_ls)
    colors = sns.color_palette("Set2", n_grp)
    sns.set_color_codes("deep")
    
    if ax is None:
        fig, ax = plt.subplots()
    
    for i,g in enumerate(grp_ls):
        sns.barplot(x='decile', y=g, data=pvt,
                    label=g, color=colors[i],
#                     label=g, color=colors[-(i+1)],
                    ax=ax)

    # Add a legend and informative axis label
    ax.legend(
        frameon=True, loc='upper center',
        bbox_to_anchor=(legend_x_pos, legend_y_pos),
        shadow=False, ncol=1
    )
    sns.despine(left=True, bottom=True)
    ax.set_title(title)
    ax.set_ylabel(y_label)
    
    return fig, ax


def plot_dual_lines_bar(
    df_lines, x, y1, y2,
    q_size, q_pct,
    pvt_bar, grp_ls, legend_y_pos=0.5,
    hline=None, title=None,
    x_opt=None,
#     **kwargs
):
    '''Integrated dual axis line plots and barplot
    '''
    # Scale pvt counts to y-axis range
    y0_max = pvt_bar.max().max()
    y_max = df_lines[y1].max()
    scaler = lambda y: y / y0_max * y_max
    
    pvt_scl = pvt_bar.copy()
    for g in grp_ls:
        pvt_scl[g] = scaler(pvt_bar[g])
    
    # Bar plot
    f, ax1 = plot_decile_stacked_bar(
        pvt_scl, grp_ls,
        legend_x_pos=1.3, legend_y_pos=legend_y_pos)
    
    # Dual line plots
    ax2 = ax1.twiny()
    f, ax3, ax4 = plot_dual_metrics(
        df_lines, x=x, y1=y1, y2=y2,
        y1_color='black', y2_color='tab:blue',
        fig=f, ax=ax2, linewidth=3, title=title,
        x_opt=x_opt,
    )
    
    if hline is not None:
        ax4.axhline(y=hline, c='tab:red', ls='--', linewidth=3)
    
    plot_x_volume_to_percent(ax4, q_size, q_pct)
    ax3.grid(False)
    ax4.grid(False)
    ax1.set_ylabel(y1)
    
    return f, ax1, ax2, ax3, ax4


def plot_multi_metrics(df, x='scr_gradient_ub',
                       metrics=['inc_sales',
                                  'target_cost', 'inc_sales_cost'],
                       title=None, xlabel=None, ylabel=None,
                       linewidth=3,
                       fig=None, ax=None,
                       x_opt=None, y_opt=None, text_opt=None, text_y_pos=None,):
    '''Plot multiple metrics using a single axis; ensure metrics are on same scale
    '''
    sns.set_palette(sns.color_palette("muted", 10))

    # text names
    t_x = x.replace('_', ' ').capitalize()
    t_y = metrics[0].replace('_', ' ').capitalize()
    t_metrics = [m.replace('_', ' ').capitalize() for m in metrics]

    if fig is None:
        fig, ax = plt.subplots()

    for i in range(len(metrics)):
        m = metrics[i]
        t_m = t_metrics[i]
        ax.plot(df[x], df[m], '-', label=t_m, linewidth=linewidth)
    ax.axvline(x=0, c='k', ls='--')

    ax.set_title(title)
    ax.set_xlabel(t_x) if xlabel is None else ax.set_xlabel(xlabel)
    ax.set_ylabel(t_y) if ylabel is None else ax.set_ylabel(ylabel)
    ax.legend(loc='upper center', bbox_to_anchor=(1.2, 1))
    ax.autoscale(enable=True, axis='x', tight=True)

    # x optimal
    if x_opt is not None:
        ax.axvline(x=x_opt, c='limegreen', ls='--', lw=3)

        if text_opt is None:
            text_opt = f'{t_x}*: {np.round(x_opt,2)}'
            if y_opt is not None:
                for j in range(len(y_opt)):
                    text_opt += f'\n{t_metrics[j]}: {np.round(y_opt[j],2)}'

        x_lims = ax.get_xlim()
        x_range = x_lims[1] - x_lims[0]
        x_delta = x_range * 0.05
        x_text_pos = x_opt + x_delta

        y_lims = ax.get_ylim()
        y_range = y_lims[1] - y_lims[0]
        y_delta = y_range * 0.05
        if text_y_pos is None:
            text_y_pos = (y_lims[0] + 0.8 * y_range) + y_delta

        ax.text(x=x_text_pos, y=text_y_pos, s=text_opt,
                fontsize=14, verticalalignment='top',
                bbox=dict(facecolor='yellow', alpha=0.3))

    return fig, ax


def plot_metrics_tile(df, is_diff=False, **kwargs):
    '''Metrics: score, inc-sales, cost, SCR

    Args:
        is_diff (bool): toggle to plot original metrics or metric diffs
    '''
#     global left, right, bottom, top, wspace, hspace
    sns.set_palette(sns.color_palette("muted", 10))

    y_score = 'score'
    y_inc_sales = 'inc_sales'
    y_cost = 'target_cost'
    y_scr = 'scr'

    if is_diff:
        y_score += '_diff'
        y_inc_sales += '_diff'
        y_cost += '_diff'
        y_scr += '_diff'

    fig, ax = plt.subplots(2, 2, figsize=(15, 9))
    fig.subplots_adjust(hspace=0.35)
#     fig.subplots_adjust(left, bottom, right, top, wspace, hspace)

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_score, ax=ax[0, 0])
    ax[0, 0].axvline(x=0, c='k', ls='--')
    ax[0, 0].set_title(y_score.capitalize())

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_inc_sales, ax=ax[0, 1])
    ax[0, 1].axvline(x=0, c='k', ls='--')
    ax[0, 1].set_title(y_inc_sales.capitalize())

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_cost, ax=ax[1, 0])
    ax[1, 0].axvline(x=0, c='k', ls='--')
    ax[1, 0].set_title(y_cost.capitalize())

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_scr, ax=ax[1, 1])
    ax[1, 1].axvline(x=0, c='k', ls='--')
    ax[1, 1].set_title(y_scr.capitalize())

    return fig


def plot_sankey(link, label, pad=10, thickness=50, title='Sankey Diagram'):
    '''Plot sankey diagram

    Args:
        link (dict): {source, target, value} input for plotly sankey diagram: integer indexes
        label (list): dictionary keys (str labels) of the link integer indexes
        pad (int): size of vertical space between bars
        thickness (int): size of horizontal width of bars
        title_text (str): title name

    Returns:
        fig (hdl): figure handle
    '''
    fig = go.Figure(data=[go.Sankey(
        node=dict(
            pad=pad,
            thickness=thickness,
            line=dict(color="white", width=0.5),
            label=label,
            #         color = ["blue", "green", 'green', 'red']
        ),
        link=link
    )])

    fig.update_layout(title_text=title, font_size=12)
    # fig.show()

    return fig