# AKL cmd and scoring functions

import importlib
import subprocess
import json

import wx_utils as utl


def path_table_joiner(
    file_base='jn_melon_crn_campaign.parquet',
    dir_cmd='mdl_20200601',
    file_join_tbls='mdl_table_selection_cmd.txt',
    file_join_cols='NA',
    cfg_name='config', cfg_path='./config.py',
):
    '''
    Paths for building table joiner: base file to CMD
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    # S3
    filename = file_base.partition('.')[0]
    filename = f'{filename}000.parquet'
    path_s3_base = f'{cfg.PATH_S3_PRJ}/{filename}'

    # GCP
    path_gcp_base = f'{cfg.PATH_GCP_DATA}/base/{file_base}'
    path_gcp_dir_cmd = f'{cfg.PATH_GCP_DATA}/w_cmd/{dir_cmd}'

    # Joiner tables and columns
    path_gcp_join_tbls = f'{cfg.PATH_GCP_CONFIG}/{file_join_tbls}'
    path_gcp_join_cols = f'{cfg.PATH_GCP_CONFIG}/{file_join_cols}'

    path_local_join_tbls = f'{cfg.PATH_LOCAL_CONFIG}/{file_join_tbls}'
    path_local_join_cols = f'{cfg.PATH_LOCAL_CONFIG}/{file_join_cols}'

    p_out = {
        'path_s3_base': path_s3_base,
        'path_gcp_base': path_gcp_base,
        'path_gcp_dir_cmd': path_gcp_dir_cmd,
        'path_gcp_join_tbls': path_gcp_join_tbls,
        'path_gcp_join_cols': path_gcp_join_cols,
        'path_local_join_tbls': path_local_join_tbls,
        'path_local_join_cols': path_local_join_cols,
    }
    return p_out


def path_akl_config(
    file_cfg_xlsx='cfg_melon_20200601_rdm.xlsx',
    file_cfg_akl='config_mdl_melon_rdm.yaml',
    file_argo='argo_mdl_melon_rdm.yaml',
    cfg_name='config', cfg_path='./config.py',
):
    ''' AKL config paths
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    # Local
    path_local_cfg_xlsx = f'{cfg.PATH_LOCAL_CONFIG}/{file_cfg_xlsx}'
    path_local_cfg_akl = f'{cfg.PATH_LOCAL_CONFIG}/akl/{file_cfg_akl}'
    path_local_argo = f'{cfg.PATH_LOCAL_CONFIG}/argo/{file_argo}'

    # GCP
    p_out = {
        'path_local_cfg_xlsx': path_local_cfg_xlsx,
        'path_local_cfg_akl': path_local_cfg_akl,
        'path_local_argo': path_local_argo,
    }
    return p_out


def path_mdl_feature_files(
    dir_mdl='redeem/2020-06-02',
    file_feat_sel='mdl_feat_sel_rdm.xlsx',
    file_mdl_smry='mdl_smry_rdm.json',
    file_feat_spec='mdl_feat_spec_rdm.xlsx',
    is_copy_mdl_files=True,
    cfg_name='config', cfg_path='./config.py',
):
    '''Paths for AKL model features to build scoring columns file
    - Return local paths of model files
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    # AKL files
    # file_gcp_mdl = 'model_selector/model_object.pickle'
    file_gcp_feat_sel = 'feature_selection/FeatureSelectionSummary.xlsx'
    file_gcp_mdl_smry = 'log/BestModelSummary.json'
    file_gcp_feat_spec = 'preprocessor/base_config_updated.xlsx'

    # GCP
    path_gcp_feat_sel = f'{cfg.PATH_GCP_MODELS}/{dir_mdl}/{file_gcp_feat_sel}'
    path_gcp_mdl_smry = f'{cfg.PATH_GCP_MODELS}/{dir_mdl}/{file_gcp_mdl_smry}'
    path_gcp_feat_spec = (
        f'{cfg.PATH_GCP_MODELS}/{dir_mdl}/{file_gcp_feat_spec}'
    )

    # Local
    path_local_feat_sel = f'{cfg.PATH_LOCAL_DATA}/{file_feat_sel}'
    path_local_mdl_smry = f'{cfg.PATH_LOCAL_DATA}/{file_mdl_smry}'
    path_local_feat_spec = f'{cfg.PATH_LOCAL_DATA}/{file_feat_spec}'

    # Copy model files from GCP to local
    if is_copy_mdl_files:
        utl.copy_gsutil(path_gcp_feat_sel, path_local_feat_sel)
        utl.copy_gsutil(path_gcp_mdl_smry, path_local_mdl_smry)
        utl.copy_gsutil(path_gcp_feat_spec, path_local_feat_spec)

    p_out = {
        'path_local_feat_sel': path_local_feat_sel,
        'path_local_mdl_smry': path_local_mdl_smry,
        'path_local_feat_spec': path_local_feat_spec,
    }
    return p_out


def crt_model_features_list(
    path_local_feat_sel,
    path_local_mdl_smry,
    path_local_feat_spec,
    path_local_scr_cols,
    path_gcp_scr_cols,
    upload=False,
    cfg_name='config', cfg_path='./config.py',
):
    '''Create model features list for scoring

    Args:
        path_local_scr_cols (str): local path to scoring columns file
        path_gcp_scr_cols (str): GCP path to scoring columns file

    Returns:
        cols_full (list): scoring columns file
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    # Read model columns
    cols = utl.get_model_cols(
        path_local_feat_sel, path_local_mdl_smry, verbose=True)

    # Map model column names back to input names
    cols = utl.map_features_mdl_to_input(
        cols, path_local_feat_spec)

    # Remove ||mflg
    cols_cln = [c.partition('||')[0] for c in cols]

    # Append COLS_INCL, remove COLS_EXCL
    cols_full = sorted(list(set(cols_cln) | set(cfg.COLS_INCL)))
    
    # Remove COLS_EXCL
    cols_sub = sorted(list(set(cols_full) - set(cfg.COLS_EXCL)))
    
#     # CMD2 Pivoter requirement: retain features starting with "fdd_"
#     r = re.compile(r"f[0-9]{2}_.*")
#     cols_sub = list(filter(r.match, cols_full))

    print(f'Number of columns original: {len(cols)}')
    print(f'Number of columns out: {len(cols_sub)}')

    # Check existence
#     np.sum(['f01_mem_8wk_spent' == c for c in cols_full])

    # Output as columns whitelist; copy to GCP config location
    with open(path_local_scr_cols, 'w') as f:
        f.write("\n".join(cols_sub))

    if upload:
        print(f'Copying column whitelist to: {path_gcp_scr_cols}...')
        utl.copy_gsutil(path_local_scr_cols, path_gcp_scr_cols)
        print('Done!')

    return cols_sub


def path_distributed_scr(
    file_dist_udf='dist_udf_scr.py',
    file_prerun='prerun.sh',
    file_requirements='requirement.txt',
    file_tar='scr_udf.tar.gz',
    file_scr_cfg='scr_config.json',
    file_scr_argo='mel_scr_argo.yaml',
    cfg_name='config', cfg_path='./config.py',
):
    '''Paths for distributed scoring
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    path_local_udf = f'{cfg.PATH_LOCAL_CONFIG}/{file_dist_udf}'
    path_local_prerun = f'{cfg.PATH_LOCAL_CONFIG}/{file_prerun}'
    path_local_req = f'{cfg.PATH_LOCAL_CONFIG}/{file_requirements}'
    path_local_tar = f'{cfg.PATH_LOCAL_CONFIG}/{file_tar}'

    path_local_scr_cfg = f'{cfg.PATH_LOCAL_CONFIG}/{file_scr_cfg}'

    path_local_argo = f'{cfg.PATH_LOCAL_CONFIG}/argo/{file_scr_argo}'

    p_out = {
        'path_local_udf': path_local_udf,
        'path_local_prerun': path_local_prerun,
        'path_local_req': path_local_req,
        'path_local_tar': path_local_tar,
        'path_local_scr_cfg': path_local_scr_cfg,
        'path_local_argo': path_local_argo,
    }
    return p_out


def crt_scr_tar_gz(
    path_local_tar,
    path_local_udf, path_local_prerun, path_local_req,
    upload=True,
    cfg_name='config', cfg_path='./config.py',
):
    '''Create distributed scoring tarball (gz) file in current
    directory and upload to GCP.
    - Temporarily copy files from config and helper folders to
    currently directory
    - Create tarball
    - Copy tarball to config folder
    - Delete temporary files in current directory
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    # Copy files to local
    subprocess.call(f'cp {path_local_udf} .', shell=True)
    subprocess.call(f'cp {path_local_prerun} .', shell=True)
    subprocess.call(f'cp {path_local_req} .', shell=True)

    # Get file names
    file_tar = path_local_tar.rpartition('/')[2]
    file_dist_udf = path_local_udf.rpartition('/')[2]
    file_prerun = path_local_prerun.rpartition('/')[2]
    file_req = path_local_req.rpartition('/')[2]

    # Create tarball
    subprocess.call(
        f"tar -zcvf {file_tar} {file_dist_udf} {file_prerun} {file_req}",
        shell=True)

    # Copy to config folder
    subprocess.call(f'cp {file_tar} {cfg.PATH_LOCAL_CONFIG}', shell=True)

    # Tarball GCP path
    path_gcp_tar = f'{cfg.PATH_GCP_CONFIG}/{file_tar}'

    if upload:
        utl.copy_gsutil(file_tar, cfg.PATH_GCP_CONFIG)

    # Delete temp files
    subprocess.call(f'rm {file_dist_udf} .', shell=True)
    subprocess.call(f'rm {file_prerun} .', shell=True)
    subprocess.call(f'rm {file_req} .', shell=True)
    subprocess.call(f'rm {file_tar} .', shell=True)

    return path_gcp_tar


def crt_scr_config(
    dir_scr,
    path_gcp_dir_cmd,
    path_gcp_tar,
    path_local_udf,
    path_local_scr_cfg,
    rm_dir=False,
    upload=True,
    cfg_name='config', cfg_path='./config.py',
):
    '''
    1. Create scr_config.json
    2. Return gcp path for scored output
    '''
    # Config name and path
    spec = importlib.util.spec_from_file_location(cfg_name, cfg_path)
    cfg = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(cfg)
    
    path_gcp_scr = f'{cfg.PATH_GCP_SCORE}/{dir_scr}'

    if rm_dir:
        try:
            utl.copy_gsutil(path_gcp_scr, option='rm')
            print('score directory removed')
        except Exception as ex:
            print(f'{ex}: score directory does not exist')

    # Paths for scr_config.json
    gcp_dir_parts = cfg.PATH_GCP_SCORE[5:].partition('/')
    filename_udf = (
        path_local_udf
        .rpartition('/')[-1]
        .partition('.')[0]
    )

    scr_config = {
        "google_project": "wx-bq-poc",
        "google_bucket": gcp_dir_parts[0],
        "google_directory": gcp_dir_parts[2],
        "run_date": dir_scr,
        "input_location": f'{path_gcp_dir_cmd[5:]}/',
        "input_type": "file",
        "udf_location": path_gcp_tar[5:],
        "udf_name": filename_udf,
        "chunks": "200",
        "udf_output_path": "output",
        "combine": "Y"
    }

    with open(path_local_scr_cfg, 'w') as f:
        json.dump(scr_config, f)

    if upload:
        utl.copy_gsutil(path_local_scr_cfg, cfg.PATH_GCP_CONFIG)

    # Processed output
    file_gcp_scored = f'combine-dist_udf_scr-{dir_scr}/processed.parquet'
    path_gcp_scored = f'{path_gcp_scr}/{file_gcp_scored}'

    return path_gcp_scored
