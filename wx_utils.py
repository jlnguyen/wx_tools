# ==============================================================
'''
SCRIPT:
	wx_utils.py
	
PURPOSE:
    - WooliesX utility functions
    
INPUTS:
	
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 24Jan2020
    ---
'''
# ==============================================================
# -------------------------------------------------

from datetime import datetime
import pandas as pd
import numpy as np
import subprocess
import pickle
import random
from operator import itemgetter
from sklearn.utils import shuffle


# https://www.kaggle.com/tilii7/hyperparameter-grid-search-with-xgboost
def timer(start_time=None):
    if not start_time:
        start_time = datetime.now()
        return start_time
    elif start_time:
        thour, temp_sec = divmod((datetime.now() - start_time).total_seconds(), 3600)
        tmin, tsec = divmod(temp_sec, 60)
        print('\n>> Time taken: %i hours %i minutes and %s seconds.' % (thour, tmin, round(tsec, 2)))
    return None


def copy_gsutil(path_in, path_out=None, option='cp'):
    
    if option == 'cp':
        print(subprocess.check_call([f'gsutil -mq cp -r {path_in} {path_out}'], shell=True))
    elif option == 'mv':
        print(subprocess.check_call([f'gsutil -mq mv -r {path_in} {path_out}'], shell=True))
    elif option == 'rm':
        print(subprocess.check_call([f'gsutil -mq rm -r {path_in}'], shell=True))
    return None


def Call_Table_Joiner(
    param_d = {
        'in_type': 'parquet' #'csv'
        ,'in_path': None #path_gcp_base_spd
        ,'out_path': None #path_gcp_cmd_spd_folder
        ,'part_format': 'yyyy-MM-dd'
        ,'join_type': 'left'
        ,'incl_tables': None #path_gcp_config_tbls
        ,'incl_cols': 'na' # path_gcp_config_cols  # 'na' # all columns
        ,'db': 'cmd'
        ,'incl_fuel': 'false'
        ,'out_file_num': '1' # *********** TOCHANGE ***********
    }):
    
    param_str = 'INPUT_TYPE={in_type},INPUT_PATH={in_path},OUTPUT_PATH={out_path},PARTITION_FORMAT={part_format},JOIN_TYPE={join_type},FILE_INCLUDED_TABLES={incl_tables},FILE_INCLUDED_COLUMNS={incl_cols},DATABASE={db},INCLUDE_FUEL={incl_fuel},OUT_FILE_NUM={out_file_num}'
    param_str = param_str.format(**param_d)
    print(param_str)
    
    # Call table joiner (to CMD)
    print(subprocess.check_output([
        'gcloud', 'dataproc', 'workflow-templates', 'instantiate', 'prd-spark-cmd-joiner', '--async', '--region', 'us-east4', '--parameters', param_str
        ], stderr=subprocess.STDOUT
    ))
    return None


def call_table_pivoter(
    in_path=None,
    in_type='parquet',
    out_path=None,
    out_type='parquet',
    csv_delimiter='|',
    partition_format='yyyy-MM-dd',
    join_type='left',
    decimal_as_double='true',
    file_incl_sources='na',
    file_incl_features='na',
    file_feature_filter='na',
    incl_fuel='false',
    no_subfolders='true',
    out_file_num='50',
    sparsity_uplimit='1.0',
):
    '''Table pivoter CMD2 v2
    Args:
        in_path (str): gcp path to base data
        out_path (str): gcp path to cmd folder
        csv_delimiter (str): csv delimiter for input file (if csv input)
        file_incl_sources (str): textfile containing list of source CMD tables;
            'na' == all tables
        file_incl_features (str): textfile containing list of source CMD features;
            'na' == all columns
    '''
    
    param_str = f"INPUT_PATH={in_path},INPUT_TYPE={in_type},OUTPUT_PATH={out_path},OUTPUT_TYPE={out_type},CSV_DELIMITER='{csv_delimiter}',PARTITION_FORMAT={partition_format},JOIN_TYPE={join_type},DECIMAL_AS_DOUBLE={decimal_as_double},FILE_INCLUDED_SOURCES={file_incl_sources},FILE_INCLUDED_FEATURES={file_incl_features},FILE_FEATURE_FILTER={file_feature_filter},INCLUDE_FUEL={incl_fuel},NO_SUBFOLDER={no_subfolders},OUT_FILE_NUM={out_file_num},SPARSITY_UPLIMIT={sparsity_uplimit}"
    print(param_str)
    
    # Call table joiner (to CMD)
    print(subprocess.check_output(
        'gcloud dataproc workflow-templates instantiate prd-spark-cmd2-pivoter-v2 --async --region us-east4 --parameters ' + param_str, stderr=subprocess.STDOUT, shell=True
    ))
    return None


def load_model(path):
    model_obj = pickle.load(open(path, 'rb'))
    return model_obj


def get_model_cols(path_feat_sel, path_mdl_smry, verbose=False):
    '''Get model features / columns
    '''
    xls = pd.ExcelFile(path_feat_sel)
    feat_sel = pd.read_excel(xls, 'Feature Records')
    feat_sel_smry = pd.read_excel(xls, 'Summary')
    mdl_smry = pd.read_json(path_mdl_smry)

    # Get # of model features
    cond = (mdl_smry['Best Model Information'] == '# of model features')
    n_feat = mdl_smry.loc[cond, 'Values'].values[0]

    # Get feature selection iteration
    cond = (feat_sel_smry['features_number'] == n_feat)
    iteration = feat_sel_smry.loc[cond, 'iteration'].values[0]

    # Get selected features (columns)
    feat_sel.fillna({'Drop at iteration':np.inf}, inplace=True)
    cond = (feat_sel['Drop at iteration'] >= iteration)
    cols = feat_sel.loc[cond, 'feature_name'].tolist()
    
    if verbose:
        print(f'Number of columns: {len(cols)}')
        
    return cols


def map_features_mdl_to_input(colLs, path_featSpec):
    '''Map feature names from model object back to input
    '''
    # Get feature mapping
    xls = pd.ExcelFile(path_featSpec)
    catg = pd.read_excel(xls, 'feature_spec_cat')
    numc = pd.read_excel(xls, 'feature_spec_num')

    featMap = dict(
        np.vstack((
            catg[['new_feature', 'feature']].values
            ,numc[['new_feature', 'feature']].values
        ))
    )
    
    # Map columns (model object back to input)
    return [v if v is not None else k
            for k,v in zip(colLs, map(featMap.get, colLs))]
        

def get_n_samples(df, n_smpl=10000):
    '''Return samples from df for df with any size rows
    '''
    return n_smpl if df.shape[0] >= n_smpl else df.shape[0]


def get_count_percent(df, grp_cols, grp_cols_pct=None):
    '''Return value counts as count and percent in columns
    - NB: NULL in grp_cols are NOT counted
    '''
    # Percent
    if grp_cols_pct is None:
        grp_pct = np.round(
            df.groupby(grp_cols).size() / 
            df.groupby(grp_cols).size().sum(), 4
        )
    else:
        grp_pct = np.round(
            df.groupby(grp_cols).size() / 
            df.groupby(grp_cols_pct).size(), 4
        )
    
    out_ = pd.concat((
        df.groupby(grp_cols).size(), grp_pct
    ), axis=1)
    out_.columns = ['cnt', 'pct']
    out_.reset_index(inplace=True)
#     out_ = pd.concat((
#         df[colname].value_counts()
#         ,df[colname].value_counts(normalize=True).round(4)
#     ),axis=1)
#     out_.columns = ['cnt', 'pct']
    
    # Append total
    tot = out_[['cnt', 'pct']].sum(axis=0).to_frame().T
    tot.rename({0: 'TOTAL'}, inplace=True)
    out_ = pd.concat((out_, tot), sort=False)
    
    return out_

    
def gcp_rename_files(path_gcp_files, rename_in, rename_out):
    '''Mass rename files in gcp bucket

    Args:
        path_gcp_files (str): path to gcp files
            e.g. gs://wx-decision-engine/data/model_data/response_back_20200415/*.parquet
        rename_in (str): string pattern to replace away
        rename_out (str): string pattern to replace with

    Returns:
        None

    Raises:
    '''
    subprocess.call(f'gsutil ls {path_gcp_files} > src-rename-list.txt', shell=True)
    subprocess.call(f"gsutil ls {path_gcp_files} | sed 's/{rename_in}/{rename_out}/g' > dest-rename-list.txt", shell=True)
    subprocess.call('paste -d " " src-rename-list.txt dest-rename-list.txt | sed -e "s/^/gsutil\ mv\ /" | while read line; do bash -c "$line"; done', shell=True)
    subprocess.call('rm src-rename-list.txt; rm dest-rename-list.txt', shell=True)

    
def bq_to_storage(spec):
    """
    write data from big query to storage
    :param spec: configuration spec in json format
    :return: file location in goodl storage
    """
    # load parameters
    project_id = spec['google_project']
    bucket_name = spec['google_bucket']
    bq_spec = spec['bq']
    dataset_id = bq_spec['dataset_id']
    table_id = bq_spec['table_id']
    folder_id = bq_spec['folder_id']
    file_id = bq_spec['file_id']
    _format = bq_spec['format']

    # load credentials
    credentials = service_account.Credentials.from_service_account_file(
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"])
    client = bigquery.Client(credentials=credentials, project=project_id)

    destination_uri = 'gs://{}/{}/{}-*.{}'.format(bucket_name, folder_id,
                                                  file_id, _format.lower())
    dataset_ref = client.dataset(dataset_id, project=project_id)
    table_ref = dataset_ref.table(table_id)

    # load job configuration
    job_config = bigquery.ExtractJobConfig()
    job_config.destination_format = _format

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        location='US',
        job_config=job_config)
    extract_job.result()  # Waits for job to complete.
    print('Exported {}:{}.{} to {}'.format(
        project_id, dataset_id, table_id, destination_uri))

    data_location = '{}/{}'.format(bucket_name, folder_id)

    return data_location


def print_logs_gcs(path):
    '''Print logs text file from GCS path
    '''
    cmd_cat = subprocess.run(f'gsutil cat {path}', shell=True, text=True, stdout=subprocess.PIPE)
    print(cmd_cat.stdout)
    return cmd_cat


def akl_roc_downsample(
    path_gcp_roc, path_local_roc,
    path_local_roc_smpl, n_smpl=5000,
    download=True, upload=False,
):
    '''Downsample roc.json in AKL classification models
    - enable plotting in AKL 0.5 diagnosis
    '''
    if download:
        copy_gsutil(path_gcp_roc, path_local_roc)

    roc = pd.read_json(path_local_roc)
    roc_T = roc.iloc[0,0]
    roc_M = roc.iloc[0,1]
    
    # Sample
    roc.iloc[0,0] = sorted(random.sample(roc_T, n_smpl), key=itemgetter(0))
    roc.iloc[0,1] = sorted(random.sample(roc_M, n_smpl), key=itemgetter(0))

    roc.to_json(path_local_roc_smpl)
    print(f'Downsampled roc.json saved to: {path_local_roc_smpl}')
    
    if upload:
        copy_gsutil(path_local_roc_smpl, path_gcp_roc)
    
    return roc


def diff_from_first(df, col='scr', col_grp='crn',
                    cols_new=['diff', 'diff_pct']):
    '''Difference in values between first rank and each of subsequent
    rows in group
    '''
    cols_new = [col + '_' + c for c in cols_new]
    col_diff = cols_new[0]
    col_diff_perc = cols_new[1]
    df[col_diff] = np.nan; df[col_diff] = np.nan

    # Append first rank row value as new column
    col_first = col + '_first'
    df = df.merge(
        (df.groupby(col_grp)[[col]].first()
         .reset_index()
         .rename(columns={col: col_first}))
        ,on=col_grp
    )
    
    # Difference between first and subsequent rows
    df[col_diff] = df[col] - df[col_first]
    df[col_diff_perc] = df[col_diff] / abs(df[col_first])

    # Remove {col_first}
    df.drop(columns=col_first, inplace=True)
    
    return df


def crt_sankey_data(src, tgt, catg_col):
    '''Merge source and target dfs to create sankey data

    Args:
        src (df): source of crn-offers (best offer)
        tgt (df): target of crn-offers (optimised SGUB offer)
        catg_col (str): category column

    Returns:
        link (dict): {source, target, value} input for plotly sankey diagram: integer indexes
        label (list): dictionary keys (str labels) of the link integer indexes
        sankey (df): {source, target, value} in df format
    '''
    trans = src.merge(tgt, on='crn', how='outer', suffixes=('_src', '_tgt'))
    sankey = trans.groupby([f'{catg_col}_src', f'{catg_col}_tgt'])['crn'].count().\
        to_frame('value').\
        rename_axis(['source', 'target']).\
        reset_index()

    # Map src and tgt elements to numbers
    k = set(sankey['source'].tolist() + sankey['target'].tolist())
    v = np.arange(0, len(k))
    sankey_dict = dict(zip(k, v))

    # Map
    sankey['source'] = sankey['source'].map(sankey_dict)
    sankey['target'] = sankey['target'].map(sankey_dict)

    # transition link
    link = dict(
        source=sankey['source'].tolist(),
        target=sankey['target'].tolist(),
        value=sankey['value'].tolist(),
    )

    # labels
    label = list(sankey_dict.keys())

    return link, label, sankey


def aggregate_metrics(
    df, metric_ls, quantum_pct=0.01, stat='mean',
    sort_by=[], sort_ascend=[],
):
    '''Aggregate metrics by quanta;
    - Calculate SCR
    - Use output to plot metrics

    Args:
        metric_ls (list) (str): metric columns to aggregate;
            metrics must exist in df
        quantum_pct (float): quantum step as percentage
        stat (str): {'mean', 'sum'}
    '''
    if len(sort_by) > 0:
        df = (
            df.sort_values(by=sort_by, ascending=sort_ascend)
            .reset_index(drop=True)
        )
    
    quantum_vol = np.int(df.shape[0] * quantum_pct)
    quanta_cnt = np.int(df.shape[0] / quantum_vol)

    # Relabel index as "volumne"
    df = df.reset_index()
    df.rename(columns={'index': 'volume'}, inplace=True)
    df['quantum'] = pd.qcut(df['volume'], q=quanta_cnt)

    # Aggregate
    stat_ls = [stat] * len(metric_ls)
    metrics_dict = dict(zip(metric_ls, stat_ls))

    if 'p_rdm' in metric_ls:
        metrics_dict['p_rdm'] = 'mean'

    agg = df.groupby('quantum').agg(metrics_dict)
    agg = agg.reset_index().reset_index()
    agg.rename(columns={'index': 'volume'}, inplace=True)
    agg['volume'] = (agg['volume'] + 1) * quantum_vol
    agg['volume_pct'] = agg['volume'] / quantum_vol * quantum_pct
    
    return agg, quantum_vol, quantum_pct, stat


def cumulate_metrics(
    _df, metric_ls, stat='sum', q_size=None,
):
    '''Get cumulative / running-total (rt) metrics

    Args:
        _df (df): aggregated metrics df
        metric_ls (list): metrics to cumulate; must be in df
        stat (str): {sum, mean} identify aggregated metrics input
        q_size (int): quantum size; use with stat=='mean'
    '''
    df = _df.copy()  # pass by value

    for m in metric_ls:
        new_col = m + '_rt'
        if stat == 'sum':
            df[new_col] = df[m].cumsum()
        elif stat == 'mean':
            df[new_col] = (
                df[m].cumsum() * q_size
                / df['volume']
            )

    return df


def select_random_group(
    df, n_rand=None, frac_rand=None,
    random_state=None
):
    '''Select random group (to accompany model
    selection)
    - Choose n_rand or frac_rand
    - Expects customer column: df['crn']
    
    Args:
        n_rand (int): random group volume
        frac_rand (float): random group fraction
    '''
    # Random sample crns
    if n_rand is not None:
        crn_smpl = pd.Series(
            df['crn'].unique()
        ).sample(np.int(n_rand), random_state=random_state)
    elif frac_rand is not None:
        crn_smpl = pd.Series(
            df['crn'].unique()
        ).sample(frac=frac_rand, random_state=random_state)

    # Random offer per sampled crn
    cond = df['crn'].isin(crn_smpl)
    df_shuffle = shuffle(df[cond], random_state=random_state)
    df_smpl = df_shuffle.groupby('crn').head(1)

    return df_smpl
    
    
